const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
var cors = require('cors');
require('dotenv/config');

app.use(cors());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

const postsRoute = require('./routes/posts');
const userRoute = require('./routes/users');

app.use('/posts', postsRoute);
app.use('/user', userRoute);

mongoose.connect(process.env.DB_CONNECT, 
    { useNewUrlParser: true, useUnifiedTopology: true }, () => {
        console.log('connected to DB');
    })

app.get('/', (req, res) => {
    res.send('Project running');
});

app.listen(3000);