const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
    username: {
        type: String,
        require: false
    },
    phone: {
        type: Number,
        require: true
    },
    otp: {
        type: Number,
        default: Math.floor(100000 + Math.random() * 900000)
    },
    created_date: {
        type: Date,
        default: Date.now()
    } 
})

module.exports = mongoose.model('Users', userSchema);