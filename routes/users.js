const express = require('express');
const router = express.Router();
const Users = require('../models/User');

router.post('/',async (req, res) => {
    const user = new Users({
        phone: req.body.phone
    });
    try {
        const singleUser = await Users.findOne({phone: req.body.phone});
        if(singleUser) {
            Users.findOneAndUpdate({phone: req.body.phone}, 
                { $set: {otp: Math.floor(100000 + Math.random() * 900000)}},
                { returnOriginal: false }).then( (data) => {
                    res.json(data);
                }).catch(err => {
                    res.json(err);
                })
        } else {
            user.save().then( () => {
                res.json(user);
            }) .catch(err => {
                res.json(err);
            })
        }

    } catch(err) {
        res.json(err);
    }
})
router.post('/otp',async (req, res) => {
    try {
        const singleUser = await Users.findOne({phone: req.body.phone, otp: req.body.otp});
        if(singleUser) {
            res.status(200).json({message: 'Login Successfull'});
        } else {
            res.status(404).json({message: 'Invalid OTP'});
        }

    } catch(err) {
        res.json(err);
    }
})

router.get('/', async (req, res) => {
    try {
        const users = await Users.find();
        res.json(users);
    } catch (err) {
        res.json(err);
    }
})

router.post('/:userId',async (req, res) => {
    try {
        const user = await Users.findById(req.params.userId);
        res.json(user);
    } catch(err) {
        res.json(err);
    }
})

module.exports = router;